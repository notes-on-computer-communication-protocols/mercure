# mercure

Websocket replacement protocol based on server-sent events. https://mercure.rocks/

# Official documentation
* https://mercure.rocks/docs

# Unofficial documentation
* [*Mercure, Server-sent Live Updates Protocol*
  ](http://phpmagazine.net/2018/12/mercure-server-sent-live-updates-protocol.html)
  2018-12 Hatem Ben Yacoub

# Related libraries
## Symfony
### Component
* [*The Mercure Component*](https://symfony.com/doc/current/components/mercure.html)

### Bundle
* [*Pushing Data to Clients Using the Mercure Protocol*
  ](https://symfony.com/doc/current/mercure.html)